#include <iostream>
#include <fstream>
#include <cctype>
#include <regex>
#include <vector>
#include <sstream>
#include <algorithm>

#include "ch8asm.hpp"

using namespace std;

void remove_comments(string &code);
vector<string> tokenize(string listing);
string get_output_filename(string name);
void swap_bits(c8 &num);

int main(int argc, char** argv) {
  if (argc != 2) {
    cout << "\nIncorrect Arguments\n\tUSAGE: ./ch8asm <assembly-file-path>\n\n";
    return 1;
  }

  string filename = argv[1], rom;
  ifstream loader(filename);

  getline(loader, rom, '\0');
  remove_comments(rom);
  loader.close();

  // By making the listing case insensitive, we only have to focus on the
  // command itself and making the process of parsing the instructions easy.
  transform(rom.begin(), rom.end(), rom.begin(), ::tolower);

  vector<string> asm_list = tokenize(rom);
  gen_label_refs(asm_list);
  vector<c8> bin_instr = to_chip8_binary(asm_list);

  ofstream fout(get_output_filename(filename), ios::binary);
  for (c8 instr : bin_instr) {

    // Due to the little endianess of x86 processors, we can't directly write
    // else the opcodes and operands will get jumbled. As a result despite
    // correctly creating instructions, they will be read incorrectly by 
    // the emulator. This has been solved by dividing each instruction in 2
    // parts which will be written in the desired order.

    uint8_t hi = (instr & 0xff00) >> 8;
    uint8_t lo = instr & 0x00ff;

    fout.write((char*)(&hi), sizeof(hi));
    fout.write((char*)(&lo), sizeof(lo));
  }
  fout.close();

  return 0;
}

void remove_comments(string &code) {

  // The role of comments is to provide further insight about the program to
  // the programmer. These are meant not to be processed as instructions and
  // hence, must be weeded out first.

  regex comment(";(.*)");
  code = regex_replace(code, comment, "");
}

vector<string> tokenize(string listing) {

  // Right now, we have one massive string that has been loaded as "rom".
  // This was done to reduce regex calls as it'll be once with this
  // implementation. Now, we need to split them into seperate assmebly
  // instructions.

  stringstream stream(listing);
  string token;
  vector<string> asm_inst;

  while (getline(stream, token))
    if (not token.empty())
      asm_inst.push_back(token);
  
  return asm_inst;
}

string get_output_filename(string name) {
  regex find_ext("\\.(.*)");
  return regex_replace(name, find_ext, ".c8");
}

void swap_bits(c8 &num) {
  uint8_t count = 15;
  c8 tmp = num;
  num >>= 1;

  while (num) {
    tmp <<= 1;
    tmp |= num & 1;
    num >>= 1;
    count--;
  }

  tmp <<= count;
  num = tmp;
}
