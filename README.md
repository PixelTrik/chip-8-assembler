# Chip-8 Assembler

A quick-and-dirty assembler made for CHIP-8 during Hack X hosted by Scaler Academy.
This project made to aid college students on understanding how assemblers work
by balancing cleverness and readability for further clarity.

## About CHIP-8
CHIP-8 is technically a high level interpreted language designed to ease the
process of programming games on home commuters from the mid 1970s like RCA
COSMAC VIP, Telmac 1800, etc. However, to write the code for CHIP-8, one has to
write in 16bit hexadecimal codes. This makes it an ideal candidate to learn how
computer architecture works either by creating an emulator or by creating an
assembler.

To build this project, simply run the included `build` script.
```
$ ./build
```

And to assemble the code, run
```
$ ./ch8asm <filename.c8asm>
```
This will result in `filename.c8` file for you to run on any CHIP-8 emulator.
You can also use the one [I built a few months
ago](https://gitlab.com/PixelTrik/chip-8).

## The assembly instructions
The assembly instructions are relatively straight forward for this taking cues
from [this wonderful technical documentation of
CHIP-8](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#8xy6). However, there
have been modifications to make the assemblers a lot more accessible:

- Command `sys` is not recognized by this assembler, this is deliberate
decision as this is a legacy feature and ignored by modern interpreters. Taking
this command off does nothing.
- Some instructions are aliased
  - Command `ld` is renamed as `mov` due to familiarity with modern assemblers
  like nasm, yasm, etc.
  - The following commands are aliased to accurately depict the functionality
  of the instruction:
    - `mov b vx` to `bcd vx` since the instruction stores the binary coded
    decimal (BCD) value of register vx in memory.
    - `mov f vx` to `dig vx` since this instruction loads the digit font of the
    value stored in register vx.
    - `mov vx k` to `wait vx` since this instruction halts the system until a
    key is pressed and the key's value is stored in register vx.

For further information on other instructions, please refer the aforementioned
documentation in this README file. And here is the full list of instructions
| | | |
| :--: | :--: | :--: |
| cls | sub vx vy | drw vx vy n |
| ret | and vx vy | chr vx |
| call label | or vx vy | bcd vx |
| jmp label | xor vx vy | skp vx |
| se vx {vy, byte} | subn vx vy | snkp vx |
| sne vx vy | shr vx {vy} | wait vx |
| mov\* | shl vx {vy} | |
| add vx {vy, byte} | rnd vx byte | |

Out of all commands, `mov` is the most versatile and hence requires more
detailed list
- `mov vx vy`
- `mov vx dt`
- `mov vx byte`
- `mov vx [i]`
- `mov i addr`
- `mov [i] vx`
- `mov dt vx`
- `mov st vx`
