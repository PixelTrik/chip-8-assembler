#pragma once

#include <unordered_map>
#include <functional>
#include <cstdint>
#include <vector>
#include <string>
using namespace std;

// This is named due to the fact that CHIP-8 is originally written as a 16-bit
// hexadecimal code.
typedef uint16_t c8;

vector<c8> to_chip8_binary(vector<string> asm_list);
void gen_label_refs(vector<string> asm_list);
