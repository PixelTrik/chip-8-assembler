#include "ch8asm.hpp"
#include <sstream>
#include <iostream>

c8 to_c8_code(string instr);
unordered_map<string, c8> label_ref;

c8 mov_v(string op1, string op2);

bool is_label(string str) {
  return str.back() == ':';
}

vector<c8> to_chip8_binary(vector<string> asm_list) {
  vector<c8> bin;
  string operation;

  for (string asm_inst : asm_list) {
    if (is_label(asm_inst))
      continue;

    c8 res = to_c8_code(asm_inst);
    bin.push_back(res);
  }

  return bin;
}

void gen_label_refs(vector<string> asm_list) {
  for (size_t x = 0; x < asm_list.size(); x++) {
    if (is_label(asm_list[x])) {
      string label = asm_list[x].substr(0, asm_list[x].size() - 1);
      label_ref[label] = 0x0200 + ((x + 2) & 0x0fff);
    }
  }
}

c8 get_reg(string reg) {
   return (reg.size() == 2) ? reg[1] - '0' : 0;
}

c8 to_addr(string str) {
  return stoi(str, 0, 16) & 0x0fff;
}

c8 to_c8_code(string instr) {
  stringstream buff(instr);
  string ops, op1, op2, op3;
  buff >> ops;
  
  if (ops == "cls")
    return 0x00e0;
  
  if (ops == "ret") 
    return 0x00ee;

  if (ops == "call") {
    buff >> op1;
    return 0x2000 | label_ref[op1];
  }

  if (ops == "jmp") {
    buff >> op1;
    c8 opcode = 0x1000;

    if (op1[0] == 'v') {
      buff >> op1;
      opcode = 0xb000;
    }
    
    return opcode | label_ref[op1];
  }

  if (ops == "se") {
    buff >> op1 >> op2;
    if (op2[0] == 'v')
      return 0x5000 | (get_reg(op1) << 8) | (get_reg(op2) << 4);
    return 0x3000 | (get_reg(op1) << 8) | to_addr(op2);
  }

  if (ops == "sne") {
    buff >> op1 >> op2;
    return 0x4000 | (get_reg(op1) << 8) | to_addr(op2);
  }

  if (ops == "mov") {
    buff >> op1 >> op2;

    switch(op1[0]) {
      case 'v' : return mov_v(op1, op2);
      case 'd' : return 0xf000 | (get_reg(op2) << 8) | 0x0015;
      case 's' : return 0xf000 | (get_reg(op2) << 8) | 0x0018;
      case '[' : return 0xf000 | (get_reg(op2) << 8) | 0x0055;
      case 'i' : return 0xa000 | to_addr(op2);
    }

    return 0x0000;
  }

  if (ops == "add") {
    buff >> op1 >> op2;
    switch(op1[0]) {
      case 'i' : return 0xf000 | (get_reg(op2) << 8) | 0x001e;
      case 'v' : 
                 if (op2[0] == 'v')
                   return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0004;
                 return 0x7000 | (get_reg(op1) << 8) | to_addr(op2);
    }
    return 0x0000;
  }

  if (ops == "or") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0001;
  }

  if (ops == "and") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0002;
  }

  if (ops == "xor") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0003;
  }

  if (ops == "sub") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0005;
  }

  if (ops == "shr") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0006;
  }

  if (ops == "subn") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x0007;
  }

  if (ops == "shl") {
    buff >> op1 >> op2;
    return 0x8000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | 0x000e;
  }

  if (ops == "rnd") {
    buff >> op1 >> op2;
    return 0xc000 | (get_reg(op1) << 8) | to_addr(op2);
  }

  if (ops == "drw") {
    buff >> op1 >> op2 >> op3;
    return 0xd000 | (get_reg(op1) << 8) | (get_reg(op2) << 4) | to_addr(op3);
  }

  if (ops == "skp") {
    buff >> op1;
    return 0xe000 | (get_reg(op1) << 8) | 0x009e;
  }

  if (ops == "snkp") {
    buff >> op1;
    return 0xe000 | (get_reg(op1) << 8) | 0x00a1;
  }

  if (ops == "wait") {
    buff >> op1;
    return 0xf000 | (get_reg(op1) << 8) | 0x000a;
  }

  if (ops == "dig") {
    buff >> op1;
    return 0xf000 | (get_reg(op1) << 8) | 0x0029;
  }

  if (ops == "bcd") {
    buff >> op1;
    return 0xf000 | (get_reg(op1) << 8) | 0x0033;
  }

  return 0x0000;
}

c8 mov_v(string op1, string op2) {
  c8 vx = get_reg(op1), opcode = 0x6000;

  switch (op2[0]) {
    case '[' : return 0xf000 | (vx << 8) | 0x0065;
    case 'd' : return 0xf000 | (vx << 8) | 0x0007;
    case 'v' : return 0x8000 | (vx << 8) | (get_reg(op2) << 4);
  }

  return opcode | (vx << 8) | to_addr(op2);
}
